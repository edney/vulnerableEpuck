package controller;

import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import jssc.SerialPortTimeoutException;

public class BluetoothHandler {

	SerialPort serialPort;
	private static int BAUDRATE = 115200;
	private boolean flagConnected;
	byte[] buffer;
	private String stringBuffer;

	public BluetoothHandler(String portName) {
		super();
		serialPort = new SerialPort(portName);
		stringBuffer = "";
	}

	public boolean connect() {

		try {

			if (serialPort != null && serialPort.isOpened()) {
				serialPort.purgePort(1);
				serialPort.purgePort(2);
				serialPort.closePort();
			}

			flagConnected = serialPort.openPort();
			while (!serialPort.isOpened()) {
				flagConnected = serialPort.openPort();
			}

			serialPort.setParams(BAUDRATE, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
			return flagConnected;

		} catch (SerialPortException ex) {
			flagConnected = false;
			return flagConnected;
		}
	}

	public boolean disconnect() {

		boolean result = true;
		try {
			while (serialPort != null && serialPort.isOpened()) {
				serialPort.purgePort(1);
				serialPort.purgePort(2);
				result = serialPort.closePort();
			}

			flagConnected = false;

		} catch (SerialPortException e) {
			e.printStackTrace();
		}
		return result;
	}

	public void sendBytes(byte[] msg) {

		try {
			serialPort.writeBytes(msg);
		} catch (SerialPortException e) {
			e.printStackTrace();
		}
	}

	public byte[] readBytes() {
		return buffer;
	}

	public String readString() throws SerialPortException {

		String str = this.serialPort.readString();
		return str;
	}

	public String getStringBuffer() {
		return stringBuffer;
	}

	public void setStringBuffer(String stringBuffer) {
		this.stringBuffer = stringBuffer;
	}

	public byte[] readBytes(int numberOfBytes) {
		byte[] realBytes = new byte[numberOfBytes];
		try {
			realBytes = this.serialPort.readBytes(numberOfBytes, 10000);

		} catch (SerialPortException | SerialPortTimeoutException e) {
			e.printStackTrace();
		}
		return realBytes;
	}

	public SerialPort getSerialPort() {
		return serialPort;
	}

	public void setSerialPort(SerialPort serialPort) {
		this.serialPort = serialPort;
	}

	public boolean isFlagConnected() {
		return flagConnected;
	}

	public void setFlagConnected(boolean flag) {
		this.flagConnected = flag;
	}
}