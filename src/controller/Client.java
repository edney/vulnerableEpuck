package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {

	Socket skt;
	static BufferedReader in;
	static PrintWriter out;

	public Client(String ip, int port) throws UnknownHostException, IOException {
		super();
		this.skt = new Socket(ip, port);
		out = new PrintWriter(skt.getOutputStream(), true);
		in = new BufferedReader(new InputStreamReader(skt.getInputStream()));
	}

	public static String request(String msg) throws IOException, InterruptedException {

		out.println(msg);
		Thread.sleep(10);
		while (!in.ready()) {}
		String response = in.readLine();
		return response;
	}

	public void disconnect() throws IOException {
		out.flush();
		out.close();
		in.close();
		skt.close();
	}
}
