package controller;

import georegression.struct.point.Point2D_F64;
import georegression.struct.point.Point2D_I32;
import georegression.struct.point.Point3D_F64;
import georegression.struct.se.Se3_F64;
import georegression.transform.se.SePointOps_F64;
import boofcv.alg.geo.PerspectiveOps;
import boofcv.struct.calib.IntrinsicParameters;

public class Fiducial {
	
	private double x;
	private double y;
	private double angle;
	Point2D_I32 center;
	private boolean active;
	private int id;
	private int khapa;
			
	public Fiducial(double x, double y, double angle) {
		super();
		this.x = x;
		this.y = y;
		this.angle = angle;
		active = false;
		
	}

	public double getX() {
		return x;
	}
	
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	public double getAngle() {
		return angle;
	}
	public void setAngle(double angle) {
		this.angle = angle;
	}
	
	public Point2D_I32 getCenter() {
		return center;
	}

	public void setCenter(Se3_F64 targetToSensor, IntrinsicParameters param, double width) {
			
		Point3D_F64 c = new Point3D_F64(0, 0,0);
		Point2D_F64 p = new Point2D_F64();
		SePointOps_F64.transform(targetToSensor,c,c);
		PerspectiveOps.convertNormToPixel(param,c.x/c.z,c.y/c.z,p);
		center = new Point2D_I32((int)(p.x+0.5),(int)(p.y+0.5));
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
		
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
		
	}

	public int getKhapa() {
		return khapa;
	}

	public void setKhapa(int khapa) {
		this.khapa = khapa;
	}
}
