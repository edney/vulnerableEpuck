package view;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.HeadlessException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ManualWindow extends JFrame {
	
	JPanel panel1;
	JPanel panel2;
	
	public ManualWindow() throws HeadlessException {
		
		final Container screen = getContentPane();
		screen.setLayout(new BoxLayout(screen,BoxLayout.Y_AXIS));
			
		panel1 = new JPanel();
		panel1.setLayout(new FlowLayout());
		panel1.setBorder(BorderFactory.createTitledBorder("Configura��o de Clientes"));
		
		JLabel ipLabel = new JLabel("Ip:");
		final JTextField ipClient = new JTextField(13);
				
		JLabel portLabel = new JLabel("Porta");
		final JTextField portClient = new JTextField(5);
		final JButton connectClient = new JButton("Conectar");
		final JButton testClient = new JButton("Testar Conex�o");
		
		JLabel ipLabel3= new JLabel("Ip:");
		JLabel vazio= new JLabel("      ");
		final JTextField ipClient3= new JTextField(13);
				
		JLabel portLabel3 = new JLabel("Porta");
		final JTextField portClient3 = new JTextField(5);
		final JButton connectClient3 = new JButton("Conectar");
		final JButton testClient3 = new JButton("Testar Conex�o");
		
		
		panel1.add(ipLabel);
		panel1.add(ipClient);
		panel1.add(portLabel);
		panel1.add(portClient);
		panel1.add(connectClient);
		panel1.add(testClient);
				
		panel1.add(ipLabel3);
		panel1.add(ipClient3);
		panel1.add(portLabel3);
		panel1.add(portClient3);
		panel1.add(connectClient3);
		panel1.add(testClient3);
			
		panel2 = new JPanel();
		panel2.setLayout(new FlowLayout());
		panel2.setBorder(BorderFactory.createTitledBorder("Configura��o do Servidor"));
		
		JLabel ipLabel2 = new JLabel("Ip:");
		final JTextField ipClient2 = new JTextField(13);
		JLabel portLabel2 = new JLabel("Porta");
		final JTextField portClient2 = new JTextField(5);
		final JButton connectClient2 = new JButton("Conectar");
		final JButton testClient2 = new JButton("Testar Conex�o");
		
		panel2.add(ipLabel2);
		panel2.add(ipClient2);
		panel2.add(portLabel2);
		panel2.add(portClient2);
		panel2.add(connectClient2);
		panel2.add(testClient2);
		
		screen.add(panel2);
		screen.add(panel1);
	}
}
