package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.ejml.UtilEjml;
import org.ejml.data.DenseMatrix64F;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.ds.openimaj.OpenImajDriver;

import boofcv.abst.fiducial.SquareImage_to_FiducialDetector;
import boofcv.factory.fiducial.ConfigFiducialImage;
import boofcv.factory.fiducial.FactoryFiducial;
import boofcv.gui.image.ImagePanel;
import boofcv.gui.image.ShowImages;
import boofcv.io.image.ConvertBufferedImage;
import boofcv.io.image.UtilImageIO;
import boofcv.struct.calib.IntrinsicParameters;
import boofcv.struct.image.ImageFloat32;
import boofcv.struct.image.ImageType;
import controller.Fiducial;
import georegression.metric.UtilAngle;
import georegression.struct.se.Se3_F64;

public class CamWindow {

	static {
		Webcam.setDriver(new OpenImajDriver());	
	}
	
	Webcam webcam;
	int cameraId;
	SquareImage_to_FiducialDetector<ImageFloat32> detector;
	IntrinsicParameters param;
	Dimension[] resolution;
	private double fiducialMinContourFraction;
	private int fiducialQuantity;
	private ArrayList<Fiducial> fiducialList;
	private ArrayList<Integer> flagUpdated;
	private int radiationRadius;
	private double sizeFiducialFactor;
	private double factorEscale;
	private double offsetX;
	private double offsetY;
	private float vulnerabilityThreshold;
		
	public CamWindow(int cameraId, Dimension dimension, int fiducialQuantity, int radiation ) {
		
		super();
		this.cameraId = cameraId;
		this.webcam = Webcam.getWebcams().get(cameraId);
		this.resolution = new Dimension[] {dimension};
		this.webcam.setCustomViewSizes(this.resolution);
		this.webcam.setViewSize(this.resolution[0]);
		this.fiducialMinContourFraction = 0.0005;
		this.sizeFiducialFactor = 100.0;
		this.factorEscale = 1.048951;
		this.offsetX = 84.5;
		this.offsetY = 48.5;
		this.fiducialQuantity = fiducialQuantity;
		this.fiducialList = new ArrayList<Fiducial>(fiducialQuantity);
		this.flagUpdated = new ArrayList<Integer>(Collections.nCopies(fiducialQuantity, 0));
		this.radiationRadius = radiation;
		this.vulnerabilityThreshold = (float) 0.5;
	}

	public void init(){
		
		this.webcam.open();
		this.param = new IntrinsicParameters();
		Dimension d = webcam.getDevice().getResolution();
		this.param.width = d.width;
		this.param.height = d.height;
		this.param.cx = d.width/2;
		this.param.cy = d.height/2;
		this.param.fx = this.param.cx/Math.tan(UtilAngle.degreeToRadian(30)); 
		this.param.fy = this.param.cx/Math.tan(UtilAngle.degreeToRadian(30));
		
		ConfigFiducialImage config = new ConfigFiducialImage();
		detector = FactoryFiducial.squareImageRobust(config, 6, ImageFloat32.class);
		List<ImageFloat32> images = new ArrayList<>();
		
		for (int i = 0; i < this.fiducialQuantity; i++) {
			images.add(UtilImageIO.loadImage("resource/" + i + ".png",ImageFloat32.class));
			detector.addPatternImage(images.get(i), 100, 7);
			fiducialList.add(new Fiducial(0, 0, 0));
		}
		
		detector.setIntrinsic(this.param);
	}
	
	public void process(){
				
		ImagePanel gui = new ImagePanel((int)this.resolution[0].getWidth(), (int)this.resolution[0].getHeight());
		ShowImages.showWindow(gui,"Augmented Epuck");

		while( true ) {
			
			BufferedImage frame = webcam.getImage();
			ImageFloat32 gray = ConvertBufferedImage.convertFrom(frame, true, ImageType.single(ImageFloat32.class));
			detector.detect(gray);
			Graphics2D g2 = frame.createGraphics();
			Se3_F64 targetToSensor = new Se3_F64();
			flagUpdated = new ArrayList<Integer>(Collections.nCopies(fiducialQuantity, 0));	
			
			for (int i = 0; i < detector.totalFound(); i++) {
				
				int index = detector.getId(i);
				detector.getFiducialToCamera(i, targetToSensor);
			
				if (RobotWindow.vulnerability.get(index).contains("S")) {
					DrawFiducial.drawSquare(targetToSensor, this.param, 7, g2, Color.RED);
				}
				else{
					DrawFiducial.drawSquare(targetToSensor, this.param, 7, g2, Color.GREEN);
				}
				
				Fiducial fiducial = new Fiducial(targetToSensor.getX() + offsetX, targetToSensor.getY() + offsetY , eulerToDegreee(matrixToEulerXYZ(targetToSensor.getRotation())[2], targetToSensor.getRotation().get(2)));
				fiducial.setCenter(targetToSensor,this.param,sizeFiducialFactor);
				fiducial.setId(index);
				fiducial.setActive(true);
				fiducialList.set(index, fiducial);
				flagUpdated.set(index, 1);
							
				DrawFiducial.drawRange(fiducialList.get(index),g2,radiationRadius);
				
				if(RobotWindow.updated){
				}
			}
			
			for (int i = 0; i < fiducialList.size(); i++) {
				if (flagUpdated.get(i) != 1) {
					fiducialList.get(i).setActive(false);
				}
			}
			
			DrawFiducial.drawRadiation(fiducialList,g2,radiationRadius);
			DrawFiducial.drawPosition(fiducialList,g2,resolution);
			gui.setBufferedImageSafe(frame);
			gui.repaint();
		}
	}

	public double getFiducialMinContourFraction() {
		return fiducialMinContourFraction;
	}

	public void setFiducialMinContourFraction(double fiducialMinContourFraction) {
		this.fiducialMinContourFraction = fiducialMinContourFraction;
	}
	
	public ArrayList<Fiducial> getFiducialList() {
		return fiducialList;
	}
	
	public void setFiducialList(ArrayList<Fiducial> fiducialList) {
		this.fiducialList = fiducialList;
	}

	public int getRadiationRadius() {
		return radiationRadius;
	}

	public void setRadiationRadius(int radiationRadius) {
		this.radiationRadius = radiationRadius;
	}

	public Dimension[] getResolution() {
		return resolution;
	}

	public void setResolution(Dimension[] resolution) {
		this.resolution = resolution;
	}

	public int getFiducialQuantity() {
		return fiducialQuantity;
	}

	public void setFiducialQuantity(int fiducialQuantity) {
		this.fiducialQuantity = fiducialQuantity;
	}

	public double getFactorEscale() {
		return factorEscale;
	}

	public void setFactorEscale(double factorEscale) {
		this.factorEscale = factorEscale;
	}
	
	public static double[] matrixToEulerXYZ( DenseMatrix64F M ) {
		      
		double[] euler = new double[3];
		double m31 = M.get( 2, 0 );
		double rotX, rotY, rotZ;
		
		if( Math.abs( Math.abs( m31 ) - 1 ) < UtilEjml.EPS ) {
			double m12 = M.get( 0, 1 );
			double m13 = M.get( 0, 2 );
		
			rotZ = 0;
			double gamma = Math.atan2( m12, m13 );
		
			if( m31 < 0 ) {
				rotY = Math.PI / 2.0;
				rotX = rotZ + gamma;
			} else {
				rotY = -Math.PI / 2.0;
				rotX = -rotZ + gamma;
			}
		} else {
			double m32 = M.get( 2, 1 );
			double m33 = M.get( 2, 2 );
		
			double m21 = M.get( 1, 0 );
			double m11 = M.get( 0, 0 );
		
			rotY = -Math.asin( m31 );
			double cosRotY = Math.cos( rotY );
			rotX = Math.atan2( m32 / cosRotY, m33 / cosRotY );
			rotZ = Math.atan2( m21 / cosRotY, m11 / cosRotY );
		}
		
		euler[0] = rotX;
		euler[1] = rotY;
		euler[2] = rotZ;
		
		return euler;
	}
	
	public static double eulerToDegreee(double eulerAngle, double signal){
		double angle;
		angle = (eulerAngle * 180.0)/Math.PI;
		
		if (angle < 0) {
			angle += 360.0;
		}
		
		if(angle > 270.0){
			
			angle = (-1) * (angle - 630.0);
		}
		else{
			angle = (-1) * (angle - 270.0);
		}
		return angle;
	}
}