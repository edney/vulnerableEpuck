package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.UnknownHostException;
import java.text.ParseException;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;

@SuppressWarnings("serial")
public class EpuckNetGUI extends JFrame{
	
	public EpuckNetGUI() throws UnknownHostException, IOException, ParseException {

		initUI();
    }

    public void initUI() throws UnknownHostException, IOException, ParseException {

        JToolBar toolbar = new JToolBar();
        
        final CamWindow camWindow = new CamWindow(0, new Dimension(1280, 720),10,400);
                      
        final RobotWindow robotWindow = new RobotWindow(camWindow,10);
        robotWindow.setSize(640,740);
        
        final ConWindow conWindow = new ConWindow();
        conWindow.setSize(570,200);
        
        final RobotWindow2 robot2 = new RobotWindow2();
        robot2.setSize(570,200);
                
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        ImageIcon camIcon = new ImageIcon("resource/cam.png");
        ImageIcon robotIcon = new ImageIcon("resource/robot.png");
        ImageIcon conIcon = new ImageIcon("resource/conection.png");
        ImageIcon manualIcon = new ImageIcon("resource/manual.png");
        ImageIcon exitIcon = new ImageIcon("resource/exit.png");
        
        JButton camButton = new JButton(camIcon);
        JButton robotButton = new JButton(robotIcon);
        JButton conButton = new JButton(conIcon);
        JButton manualButton = new JButton(manualIcon);
        JButton exitButton = new JButton(exitIcon);

        toolbar.add(camButton);
        toolbar.add(robotButton);
        toolbar.add(conButton);
        toolbar.add(manualButton);
        toolbar.add(exitButton);
        toolbar.setAlignmentX(0);
                        
        camButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
            	
            	new Thread() {
					@Override
					public void run() {
						camWindow.init();
		            	camWindow.process();	
					}
				}.start();
		    }
        });
        
        conButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
            	
            	new Thread() {
					@Override
					public void run() {
						robot2.setVisible(true);
					}
				}.start();
		    }
        });
               
        robotButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new Thread() {
					@Override
					public void run() {
						robotWindow.setVisible(true);
						try {
							robotWindow.process();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}	
					}
				}.start();
			}
		});
                       
        panel.add(toolbar);
        add(panel, BorderLayout.NORTH);
        setTitle("EpuckNet");
        setSize(340, 100);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        
    	SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                EpuckNetGUI ex = null;
				try {
					ex = new EpuckNetGUI();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				}
                ex.setVisible(true);
            }
        });
    }
}