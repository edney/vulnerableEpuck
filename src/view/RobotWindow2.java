package view;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.HeadlessException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class RobotWindow2 extends JFrame {
	
	JPanel panel1;
	JPanel panel2;
	
	public RobotWindow2() throws HeadlessException {
		
		final Container screen = getContentPane();
		screen.setLayout(new BoxLayout(screen,BoxLayout.Y_AXIS));
			
		panel1 = new JPanel();
		panel1.setLayout(new FlowLayout());
		panel1.setBorder(BorderFactory.createTitledBorder("Configuração dos E-pucks"));
		
		ImageIcon robotimg = new ImageIcon("resource/epuck.png");
		JButton robot = new JButton(robotimg);
		robot.setText("2948");
		robot.setBackground(Color.GREEN);
		final JButton connect = new JButton("Desconectar");
		connect.setBackground(Color.GREEN);
		panel1.add(robot);
		panel1.add(connect);
		
		JButton robot1 = new JButton(robotimg);
		robot1.setText("3167");
		robot1.setBackground(Color.RED);
		final JButton connect1 = new JButton("   Conectar ");
		connect1.setBackground(Color.RED);
		panel1.add(robot1);
		panel1.add(connect1);
		
		JButton robot2 = new JButton(robotimg);
		robot2.setText("3080");
		robot2.setBackground(Color.RED);
		final JButton connect2 = new JButton("   Conectar ");
		connect2.setBackground(Color.RED);
		panel1.add(robot2);
		panel1.add(connect2);

		JButton robot3 = new JButton(robotimg);
		robot3.setText("3040");
		robot3.setBackground(Color.RED);
		final JButton connect3 = new JButton("   Conectar ");
		connect3.setBackground(Color.RED);
		panel1.add(robot3);
		panel1.add(connect3);
		
		JButton robot4 = new JButton(robotimg);
		robot4.setText("3118");
		robot4.setBackground(Color.RED);
		final JButton connect4 = new JButton("   Conectar ");
		connect4.setBackground(Color.RED);
		panel1.add(robot4);
		panel1.add(connect4);

		JButton robot5 = new JButton(robotimg);
		robot5.setText("3177");
		robot5.setBackground(Color.GREEN);
		final JButton connect5 = new JButton("Desconectar");
		connect5.setBackground(Color.GREEN);
		panel1.add(robot5);
		panel1.add(connect5);
		
		JButton robot6 = new JButton(robotimg);
		robot6.setText("3054");
		robot6.setBackground(Color.RED);
		final JButton connect6 = new JButton("   Conectar ");
		connect6.setBackground(Color.RED);
		panel1.add(robot6);
		panel1.add(connect6);
	
		JButton robot7 = new JButton(robotimg);
		robot7.setText("2816");
		robot7.setBackground(Color.GREEN);
		JButton disconnect7 = new JButton("Desconectar");
		disconnect7.setBackground(Color.GREEN);
		panel1.add(robot7);
		panel1.add(disconnect7);
		
		JButton robot8 = new JButton(robotimg);
		robot8.setText("2926");
		robot8.setBackground(Color.GREEN);
		JButton disconnect8 = new JButton("Desconectar");
		disconnect8.setBackground(Color.GREEN);
		panel1.add(robot8);
		panel1.add(disconnect8);
	
		JButton robot9 = new JButton(robotimg);
		robot9.setText("2979");
		robot9.setBackground(Color.GREEN);
		JButton disconnect9 = new JButton("Desconectar");
		disconnect9.setBackground(Color.GREEN);
		panel1.add(robot9);
		panel1.add(disconnect9);
		
		panel2 = new JPanel();
		panel2.setLayout(new FlowLayout());
		panel2.setBorder(BorderFactory.createTitledBorder("Parâmetros Gerais"));
		
		JLabel range = new JLabel("Range Sensores:");
		JTextField fieldrange = new JTextField(5);
		JButton enviarRange = new JButton("Enviar");
		
		JLabel fator = new JLabel("Fator de Escala:");
		JTextField fieldfator = new JTextField(5);
		JButton enviarfator = new JButton("Enviar");
		
		JLabel limiar = new JLabel("Limiar de Vulnerabilidade:");
		JTextField fieldlimiar = new JTextField(5);
		JButton enviarlimiar = new JButton("Enviar");
		
		panel2.add(range);
		panel2.add(fieldrange);
		panel2.add(enviarRange);
		panel2.add(fator);
		panel2.add(fieldfator);
		panel2.add(enviarfator);
		panel2.add(limiar);
		panel2.add(fieldlimiar);
		panel2.add(enviarlimiar);
		
		screen.add(panel2);
		screen.add(panel1);
	}
}
